# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from string import digits, ascii_uppercase

DIGITS = digits + ascii_uppercase[:22]


def toBase32hex(number: int) -> str:
    res = ""
    if number == 0:
        return "0"

    while number > 0:
        reminder = number % 32
        res = DIGITS[reminder] + res
        number = number // 32

    return res
